LF_NEWLINE = '\n'
CRLF_NEWLINE = '\r\n'


def _check_win_newline(file_):
    """ Worker function for checikng everyline of file """
    f = open_file(file_)
    for line in f:
        if not CRLF_NEWLINE in line:
            return False
    return True


def is_win_file(file_):
    """ Check if file is correct Windows file """
    return _check_win_newline(file_)


def open_file(file_):
    """ Open file respecting newline chars """
    return open(file_, 'rb')


def _get_changed(file_list):
    """ Return list of files added and modified """
    return file_list[0] + file_list[1]


def newline_hook(ui, repo, **kwargs):
    """
    Mercurial hook for checking if all commited files have only Windows
    newline chars.
    On failure list of files with incorrect newlines is printed
    to stdout.

    How to configure your mercurial?
    [hooks]
    precommit = python:/path/to/file/newline.py:newline_hook

    """
    file_list = []
    for file_ in _get_changed(repo.status()):
        if not is_win_file(file_):
            file_list.append(file_)

    if file_list:
        print 'Commit aborted.'
        print 'Detected newline chars other than CRLF.'
        print 'File list: %s' % file_list
        return True

    return False
