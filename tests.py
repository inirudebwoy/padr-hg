"""
Tests for hg plugin for newline check.
Run with nose (https://nose.readthedocs.org).

"""
from newline import _check_win_newline, is_win_file


def test_unix_newline():
    """
    Test checking if UNIX newline (LF) is
    correctly recognized.

    """
    assert _check_win_newline('unix_file.txt') == False


def test_windows_newline():
    """
    Test checking if Windows newline (CRLF) is
    correctly recognized.

    """
    assert _check_win_newline('windows_file.txt') == True


def test_wrapper_windows():
    assert is_win_file('windows_file.txt') == True
